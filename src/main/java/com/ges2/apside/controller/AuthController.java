package com.ges2.apside.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import test.springsecurity.springsecurity.Dto.EmployeeDto;
import test.springsecurity.springsecurity.entity.Employee;
import test.springsecurity.springsecurity.service.EmployeeService;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class AuthController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EmployeeService employeeService;


    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/register")
    public String registrationForm(Model model) {
        EmployeeDto employeeDto = new EmployeeDto();

        return "register";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @PostMapping("/register/save")
    public String registration(@Valid @ModelAttribute("employee") EmployeeDto employeeDto,
                               BindingResult result,
                               Model model){
        Employee existingUser = employeeService.findByLogin(employeeDto.getLogin());

        if(existingUser != null && existingUser.getEmail() != null && !existingUser.getEmail().isEmpty()){
            result.rejectValue("email", null,
                    "There is already an account registered with the same email");
        }

        if(result.hasErrors()){
            model.addAttribute("employee", employeeDto);
            return "/register";
        }

        employeeService.save(employeeDto);
        return "redirect:/register?success";
    }

}
