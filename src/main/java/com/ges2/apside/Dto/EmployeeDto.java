package com.ges2.apside.Dto;


import test.springsecurity.springsecurity.entity.Phase;
import test.springsecurity.springsecurity.entity.Role;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class EmployeeDto {

    private int id;
    @NotNull
    @NotEmpty(message = "File number is required")
    private String fileNumber;
    @NotNull
    @NotEmpty(message = "Lastname is required")
    private String lastName;

    @NotNull
    @NotEmpty(message = "Firstname is required")
    private String firstName;

    @NotNull
    @NotEmpty(message = "Phone number is required")
    private String phoneNumber;

    @NotNull
    @NotEmpty(message = "Email is required")
    private String email;

    @NotNull
    @NotEmpty(message = "Login is required")
    private String login;

    @NotNull
    @NotEmpty(message = "Password is required")
    private String password;

    @NotNull
    @NotEmpty(message = "Created at is required")
    private String createdAt;
    private String updatedAt;

    @NotNull
    @NotEmpty(message = "Role is required")
    private Role role;
    private Set<Phase> phases = new HashSet<>();

    public EmployeeDto() {
        super();
    }

    public EmployeeDto(
            int id, String fileNumber, String lastName, String firstName, String phoneNumber, String email,
            String login, String password, String createdAt, String updatedAt,
            Role role, Set<Phase> phases) {
        this.id = id;
        this.fileNumber = fileNumber;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.login = login;
        this.password = password;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.role = role;
        this.phases = phases;
    }

    public EmployeeDto(
            String fileNumber, String lastName, String firstName, String phoneNumber, String email,
            String login, String password, String createdAt, String updatedAt,
            Role role, Set<Phase> phases) {
        this.fileNumber = fileNumber;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.login = login;
        this.password = password;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.role = role;
        this.phases = phases;
    }

    public int getId() {
        return id;
    }

    public EmployeeDto setId(int id) {
        this.id = id;
        return this;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public EmployeeDto setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public EmployeeDto setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public EmployeeDto setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public EmployeeDto setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public EmployeeDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public EmployeeDto setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public EmployeeDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public EmployeeDto setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public EmployeeDto setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Role getRole() {
        return role;
    }

    public EmployeeDto setRole(Role role) {
        this.role = role;
        return this;
    }

    public Set<Phase> getPhases() {
        return phases;
    }

    public EmployeeDto setPhases(Set<Phase> phases) {
        this.phases = phases;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeDto entity = (EmployeeDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.fileNumber, entity.fileNumber) &&
                Objects.equals(this.lastName, entity.lastName) &&
                Objects.equals(this.firstName, entity.firstName) &&
                Objects.equals(this.phoneNumber, entity.phoneNumber) &&
                Objects.equals(this.email, entity.email) &&
                Objects.equals(this.login, entity.login) &&
                Objects.equals(this.password, entity.password) &&
                Objects.equals(this.createdAt, entity.createdAt) &&
                Objects.equals(this.updatedAt, entity.updatedAt) &&
                Objects.equals(this.role, entity.role) &&
                Objects.equals(this.phases, entity.phases);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                fileNumber,
                lastName, firstName,
                phoneNumber,
                email,
                login,
                password,
                createdAt,
                updatedAt,
                role,
                phases
        );
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "fileNumber = " + fileNumber + ", " +
                "lastName = " + lastName + ", " +
                "firstName = " + firstName + ", " +
                "phoneNumber = " + phoneNumber + ", " +
                "email = " + email + ", " +
                "login = " + login + ", " +
                "password = " + password + ", " +
                "createdAt = " + createdAt + ", " +
                "updatedAt = " + updatedAt + ", " +
                "role = " + role + ", " +
                "phases = " + phases + ")";
    }
}
