package com.ges2.apside.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "organization")
public class Organization {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "addr_web")
    private String addrWeb;

    @Column(name = "contact_email")
    private String contactEmail;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "name")
    private String name;


    @Column(name = "phone_number")
    private int phoneNumber;

    @OneToMany(mappedBy = "organization")
    private List<Project> projects;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;


    public List<Project> getProjects() {
        return projects;
    }

    public Organization setProjects(List<Project> projects) {
        this.projects = projects;
        return this;
    }

    public Project addProject(Project project) {
        getProjects().add(project);
        project.setOrganization(this);

        return project;
    }

    public Project removeProject(Project project) {
        getProjects().remove(project);
        project.setOrganization(null);

        return project;
    }

}
