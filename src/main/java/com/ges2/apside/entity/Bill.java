package com.ges2.apside.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bill")
public class Bill {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	
     @Column(name = "code", nullable = false)
     private String code;
     
     @Column(name = "amount")
     private double amount;

     @Column(name = "status")
     private String status;

     @ManyToOne
     @JoinColumn(name = "phase_id")
     private Phase phase;


}