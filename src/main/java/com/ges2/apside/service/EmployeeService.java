package com.ges2.apside.service;

import com.ges2.apside.Dto.EmployeeDto;
import com.ges2.apside.entity.Employee;


import java.util.List;

public interface EmployeeService {

    void save(EmployeeDto employeeDto);

    Employee findByEmail(String email);

    Employee findByLogin(String login);

    List<EmployeeDto> findAll();

    void delete(int id);

    Employee findById(int id);
}
