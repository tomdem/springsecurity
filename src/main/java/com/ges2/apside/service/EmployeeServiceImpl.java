package com.ges2.apside.service;

import com.ges2.apside.Dto.EmployeeDto;
import com.ges2.apside.entity.Employee;
import com.ges2.apside.entity.Role;
import com.ges2.apside.repository.RoleRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.ges2.apside.repository.EmployeeRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               RoleRepository roleRepository,
                               BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.employeeRepository = employeeRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void save(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        employee.setLogin(employeeDto.getLogin());
        employee.setFirstName(employeeDto.getFirstName() + " " + employeeDto.getLastName());
        employee.setPassword(bCryptPasswordEncoder.encode(employeeDto.getPassword()));
        employee.setEmail(employeeDto.getEmail());

        Role role = roleRepository.findByName("ROLE_USER");

        employee.setRoles(
                employeeDto.getRole().equals(role) ?
                Arrays.asList(role, roleRepository.findByName("ROLE_ADMIN")) :
                Collections.singletonList(role));

        employeeRepository.save(employee);
    }

    @Override
    public Employee findByEmail(String email) {
        return employeeRepository.findByEmail(email);
    }

    @Override
    public Employee findByLogin(String login) {
        return employeeRepository.findByLogin(login);
    }

    @Override
    public List<EmployeeDto> findAll() {
        List<Employee> employees = employeeRepository.findAll();
        return employees.stream()
                .map(this::mapToEmployee)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(int id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee findById(int id) {
        return null;
    }

    private EmployeeDto mapToEmployee(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setLogin(employee.getLogin());
        employeeDto.setEmail(employee.getEmail());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());

        return employeeDto;
    }
}