package com.ges2.apside.repository;

import com.ges2.apside.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
        Role findByName(String name);
}

